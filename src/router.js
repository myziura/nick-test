import Vue from 'vue'
import Router from 'vue-router'
import ViewTodo from '@/views/ViewTodo'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: ViewTodo
    }
  ]
})